package com.example.jefffragments.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jefffragments.R
import com.example.jefffragments.databinding.FragmentGreenBinding



class GreenFragment : Fragment(R.layout.fragment_green) {
    private var _binding: FragmentGreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGreenBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root


}